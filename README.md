Frontend test v3
===========

## TASK #1

* Simplify/refactor this function as much as you can

```js
function func(s, a, b) {
    var match_empty=/^$/ ;
    if (s.match(match_empty)) {
        return -1;
    } else {
        var i=s.length-1;
        var aIndex=-1;
        var bIndex=-1;

        while ((aIndex==-1) && (bIndex==-1) && (i>=0)) {
            if (s.substring(i, i+1) == a) {
	 				aIndex = i;
 				}
        		if (s.substring(i, i+1) == b) {
	 				bIndex = i;
 				}
        		i--;
        }

        if (aIndex != -1) {
            if (bIndex == -1) {
	 				return aIndex;
 				} else {
					return Math.max(aIndex, bIndex);
				}
        } else {
            if (bIndex != -1) {
	 				return bIndex;	 
 				} else {
					return -1;
				}
        }
    }
};
```

## TASK #2

* Run index.html and understand how it works
* Open and examine index.html
* Make a facepalm
* Refactor it, as good as possible. You can do whatever you want - clean, remove, separate, use a framework or technology, or keep it simple.
It is up to you
* Write a clean code: developer, which will work after you, should not have a desire to kill you
* Use best practices when creating UI: semantic, adaptive UI, availability
* Browsers: Latest green
* Put your solution into git repo (you can use github or bitbucket)
